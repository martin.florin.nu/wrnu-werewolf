using System.Text.RegularExpressions;
using Wrnu.Were.Model;

namespace Wrnu.Were;

public record WereVote(string Voter, int PostPosition, int PostId, string Vote)
{
    public static WereVote? Parse(ForumPost post, string searchAttribute, bool simpleSearch = false)
    {
        var voter = post.username;
        var postPosition = post.position+1;
        var postId = post.post_id;
        var cleanedMessage = post.message
                .Replace("[B]", "", StringComparison.InvariantCultureIgnoreCase)
                .Replace("[/B]", "", StringComparison.InvariantCultureIgnoreCase);

        var quoteregex = new Regex(@"\[QUOTE(.*?)/QUOTE\]", RegexOptions.Singleline | RegexOptions.Multiline);

        cleanedMessage = "\n" + quoteregex.Replace(cleanedMessage, "removedQuote") + "\n";
		
        var listOfRegexes = new List<Regex>();
		
        listOfRegexes.Add(new Regex(@"(\n)?(" + searchAttribute + @")(:)?[ ]*(\[USER=[0-9]+\]@)(?<name>[a-zA-Z0-9åäöÅÄÖ_\- ]+)(\[\/USER\])(\n)?", RegexOptions.IgnoreCase));
        if (simpleSearch)
        {
            listOfRegexes.Add(new Regex(@"(\n)?" + searchAttribute + @":[ ]+(?<name>[a-zA-Z0-9åäöÅÄÖ_\- ]+)[ ]*(\n)?", RegexOptions.IgnoreCase));
        }

        foreach (var regex in listOfRegexes)
        {
            var match = regex.Matches(cleanedMessage);

            if (match.Any())
            {

                var lastMatch = match.Last();
                var message = lastMatch.Groups["name"];

                return new WereVote(voter, postPosition, postId, message.Value.Trim());
            }
        }

        return null;
    }

    public string GetPostLink(int threadId)
    {
        return $"https://rollspel.nu/threads/{threadId}/post-{PostId}";
    }
}