﻿using System.Diagnostics;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Caching.Memory;
using System.Text.RegularExpressions;
using Wrnu.Were.Model;

namespace Wrnu.Were;

public class WrnuRestApi
{
	private readonly HttpClient _client;
	private readonly IMemoryCache _cache;

	//private Regex gameNameRegex = new Regex(@"(?<nameofgame>.*?)[, -]*[\[\{\(]?( )?\d?( )?R(unda|undan)?[\]\}\)]?[ ]*[\[\{\(]?\d?[\]\}\)]?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
	private Regex gameNameRegex = new Regex(@"(?<nameofgame>.*?)[, -]( )?\d?( )?R(unda|undan)?[ ]*\d?", RegexOptions.IgnoreCase | RegexOptions.Compiled);
	private Dictionary<string, string> replaceNames;
	
	public WrnuRestApi(HttpClient client, IMemoryCache cache)
	{
		_client = client ?? throw new ArgumentNullException(nameof(client));
		_cache = cache;
		replaceNames = new Dictionary<string, string>();
		replaceNames.Add("[", "");
		replaceNames.Add("]", "");
		replaceNames.Add("{", "");
		replaceNames.Add("}", "");
		replaceNames.Add("(", "");
		replaceNames.Add(")", "");
		replaceNames.Add("första", "1");
		replaceNames.Add("andra", "2");
		replaceNames.Add("tredje", "3");
		replaceNames.Add("fjärde", "4");
		replaceNames.Add("femte", "5");
		replaceNames.Add("sjätte", "6");
		replaceNames.Add("sjunde", "7");
		replaceNames.Add("åttonde", "8");
		replaceNames.Add("nionde", "9");
		replaceNames.Add("tionde", "10");
		replaceNames.Add("timme", "runda");
	}

	public async Task<IReadOnlyList<ForumThread>> GetThreads(int forum)
	{
		var threads = new List<ForumThread>();
		var lastThreadPage = _cache.GetOrCreate($"lastPageThread-{forum}", _ => 1);

		int i = lastThreadPage;
		if (_cache.TryGetValue<List<ForumThread>>($"threads-{forum}", out var cachedThreads))
		{
			Debug.WriteLine($"Forum: {forum}, cached page: 1-{lastThreadPage}");
			threads.AddRange(cachedThreads);
		}
		
		while (true)
		{
			var response = await _client.GetFromJsonAsync<ForumThreadResponse>($"forums/{forum}/threads?page={i}");

			if (response?.threads is not null)
			{
				threads.AddRange(response.threads);
			}

			if (response?.pagination is null)
			{
				break;
			}
			
			if (response.pagination.current_page == response.pagination.last_page)
			{
				break;
			}

			i = response.pagination.current_page + 1;
		}
		
		_cache.Set($"threads-{forum}", threads);
		_cache.Set($"lastPageThread-{forum}", i);
		
		return threads;
	}

	public async Task<List<Game>> GetGames(int forum)
	{
		var threads = await GetThreads(forum);

		var listOfGameNames = new Dictionary<string, List<ForumThread>>();
		
		foreach (var thread in threads)
		{
			var title = CleanUpThreadName(thread.title);
			var matches = gameNameRegex.Match(title); 
			if (matches.Success)
			{
				string nameOfGame = matches.Groups["nameofgame"].Value.Trim();
				if (string.IsNullOrWhiteSpace(nameOfGame))
				{
					continue;
				}
				
				if (listOfGameNames.Any(item => item.Key.Equals(nameOfGame, StringComparison.InvariantCultureIgnoreCase)))
				{
					listOfGameNames.First(item => item.Key.Equals(nameOfGame, StringComparison.InvariantCultureIgnoreCase)).Value.Add(thread);
					continue;
				}
				listOfGameNames.Add(nameOfGame, new List<ForumThread>(){ thread });
			}
			else
			{
				Console.WriteLine($"Failed game: {title}");
			}
		}

		var games = new List<Game>();
		
		foreach (var gameName in listOfGameNames)
		{
			games.Add(new Game(gameName.Key, gameName.Value.FirstOrDefault()?.username, gameName.Value, gameName.Value.OrderBy(s => s.post_date).First().Created));
		}

		return games;
	}

	private string CleanUpThreadName(string threadTitle)
	{
		var resultTitle = threadTitle;
		foreach (var replaceName in replaceNames)
		{
			resultTitle = resultTitle.Replace(replaceName.Key, replaceName.Value, StringComparison.InvariantCultureIgnoreCase);
		}
		return resultTitle;
	}

	public async Task<IReadOnlyList<ForumPost>> GetThread(int threadId)
	{
		var posts = new List<ForumPost>();
		int i = 1;
		while (true)
		{
			var response = await _client.GetFromJsonAsync<ForumPostResponse>($"threads/{threadId}/posts?page={i}");

			if (response?.posts is not null)
			{
				posts.AddRange(response.posts);
			}

			if (response?.pagination is null)
			{
				break;
			}
			
			if (response.pagination.current_page == response.pagination.last_page)
			{
				break;
			}

			i = response.pagination.current_page + 1;
		}
		
		return posts;
	}
	
	public async Task<List<WereVote>> GetVotes(int threadId, string searchAttribute, bool simpleSearch = false)
	{
		var posts = await GetThread(threadId);

		return ExtractVotesFromPosts(posts, searchAttribute, simpleSearch);
	}

	private List<WereVote> ExtractVotesFromPosts(IReadOnlyList<ForumPost> posts, string searchAttribute, bool simpleSearch)
	{
		var votes = new List<WereVote>();
		foreach (var post in posts)
		{
			if (post.position == 0)
			{
				continue;
			}
			
			var vote = WereVote.Parse(post, searchAttribute, simpleSearch);

			if (vote is not null)
			{
				votes.Add(vote);
			}
		}
		return votes.OrderBy(vote => vote.PostPosition).ToList();
	}
	
	private string[] GetAttributes(IReadOnlyList<ForumPost> posts)
	{
		var defaultResult = new[]{ "Röst" };
		
		var firstPost = posts.FirstOrDefault(p => p.position == 0)?.message;

		if (firstPost is null)
		{
			return defaultResult;
		}
		
		var regex = new Regex(@"\[(?<attributes>[a-zA-Z0-9åäöÅÄÖ_\- ,]+)\](\[\/[a-zA-Z]+\])*$");

		if (regex.IsMatch(firstPost))
		{
			var listOfAttributesString = regex.Match(firstPost).Groups["attributes"].Value;

			var listOfAttributes = listOfAttributesString.Split(",").Select(str => str.Trim());

			return listOfAttributes.ToArray();
		}

		return defaultResult;
}

	public async Task<List<VoteSummary>> GetVoteSummaries(int threadId, string searchAttribute, bool simpleSearch = false)
	{
		var votes = await GetVotes(threadId, searchAttribute, simpleSearch);

		return GetCalculatedVotes(votes);
		
		
	}
	
	public async Task<List<VoteExtendedSummary>> GetExtendedVotes(int threadId, string searchAttribute, bool simpleSearch)
	{
		var votes = await GetVotes(threadId, searchAttribute, simpleSearch);

		return GetCalculatedExtendedVotes(votes, threadId);
		
		
	}

	public List<VoteExtendedSummary> GetCalculatedExtendedVotes(List<WereVote> votes, int threadId)
	{
		var voters = votes.Select(vote => vote.Voter).Distinct(StringComparer.InvariantCultureIgnoreCase);

		var legalVotes = new List<WereVote>();
		foreach (var voter in voters)
		{
			legalVotes.Add(votes.Where(vote => vote.Voter == voter).OrderByDescending(vote => vote.PostPosition).First());
		}
		
		var votedOn = legalVotes.Select(vote => vote.Vote).Distinct(StringComparer.InvariantCultureIgnoreCase);

		var voteSummaries = new List<VoteExtendedSummary>();
		
		foreach (var voted in votedOn)
		{
			voteSummaries.Add(new VoteExtendedSummary(voted)
			{
				votes = 0,
				firstVotePosition = Int32.MaxValue
			});
		}

		foreach (var voteSummary in voteSummaries)
		{
			var votesOnThis = legalVotes.Where(vote => vote.Vote.Equals(voteSummary.name, StringComparison.InvariantCultureIgnoreCase)).ToList();
			
			voteSummary.votes = votesOnThis.Count;
			voteSummary.firstVotePosition = votesOnThis.Min(vote => vote.PostPosition);
			voteSummary.voters = votesOnThis.Select(vote => new Voter(vote.Voter, vote.PostPosition, vote.GetPostLink(threadId))).ToList();

		}

		return voteSummaries.OrderByDescending(vote => vote.votes).ThenBy(vote => vote.firstVotePosition).ToList();
	}
	
	public List<VoteSummary> GetCalculatedVotes(List<WereVote> votes)
	{
		var voters = votes.Select(vote => vote.Voter).Distinct(StringComparer.InvariantCultureIgnoreCase);

		var legalVotes = new List<WereVote>();
		foreach (var voter in voters)
		{
			legalVotes.Add(votes.Where(vote => vote.Voter == voter).OrderByDescending(vote => vote.PostPosition).First());
		}
		
		var votedOn = legalVotes.Select(vote => vote.Vote).Distinct(StringComparer.InvariantCultureIgnoreCase);

		var voteSummaries = new List<VoteSummary>();
		
		foreach (var voted in votedOn)
		{
			voteSummaries.Add(new VoteSummary(voted)
			{
				votes = 0,
				firstVotePosition = Int32.MaxValue
			});
		}

		foreach (var voteSummary in voteSummaries)
		{
			var votesOnThis = legalVotes.Where(vote => vote.Vote.Equals(voteSummary.Name, StringComparison.InvariantCultureIgnoreCase)).ToList();
			
			voteSummary.votes = votesOnThis.Count;
			voteSummary.firstVotePosition = votesOnThis.Min(vote => vote.PostPosition);
			voteSummary.voters = string.Join(", ", votesOnThis.OrderBy(vote => vote.PostPosition).Select(vote => $"{vote.Voter} ({vote.PostPosition})"));

		}

		return voteSummaries.OrderByDescending(vote => vote.votes).ThenBy(vote => vote.firstVotePosition).ToList();
	}

	public async Task<List<VoteResult>> GetCustomVoteSummaries(int threadId, string[]? attributes, bool simpleSearch)
	{
		var posts = await GetThread(threadId);

		if (attributes is null || attributes.Length == 0)
		{
			attributes = GetAttributes(posts);
		}

		var list = new List<VoteResult>();
		
		foreach (var attribute in attributes)
		{
			var votes = ExtractVotesFromPosts(posts, attribute, simpleSearch);
			
			var result = GetCalculatedVotes(votes);
			
			list.Add(new VoteResult()
			{
				Name = attribute,
				VoteSummary = result,
				Posts = votes
			});
		}

		return list;
	}
}

public record VoteResult
{
	public string Name { get; set; } = "N/A";

	public List<VoteSummary> VoteSummary { get; set; } = new List<VoteSummary>();

	public List<WereVote> Posts { get; set; } = new List<WereVote>();
}