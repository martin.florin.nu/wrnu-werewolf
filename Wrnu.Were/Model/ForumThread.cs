namespace Wrnu.Were.Model;

public record ForumThread(string title, int thread_id, int reply_count, string username, int last_post_id, long post_date) : IReadable
{
    public string FormattedString()
    {
        return $"{title}";
    }

    public DateTime? Created => DateTimeOffset.FromUnixTimeMilliseconds(post_date).DateTime;
}

public interface IReadable
{
    string FormattedString();
}