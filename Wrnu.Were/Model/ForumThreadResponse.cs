namespace Wrnu.Were.Model;

public class ForumThreadResponse : Paginated
{
    public ForumThreadResponse()
    {
        threads = new List<ForumThread>();
    }

    public List<ForumThread> threads { get; set; }
}