namespace Wrnu.Were.Model;

public record Voter(string name, int id, string link);