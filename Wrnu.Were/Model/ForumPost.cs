namespace Wrnu.Were.Model;

public record ForumPost(string message, string username,  int post_id, int position)
{
}