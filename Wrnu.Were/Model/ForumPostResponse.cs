namespace Wrnu.Were.Model;

public class ForumPostResponse: Paginated
{
    public ForumPostResponse()
    {
        posts = new List<ForumPost>();
    }

    public List<ForumPost> posts { get; set; }
}