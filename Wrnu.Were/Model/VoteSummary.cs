namespace Wrnu.Were.Model;

public record VoteSummary(string Name)
{
    public int votes { get; set; } = 0;

    public int firstVotePosition { get; set; } = -1;
	
    public string? voters { get; set; }
}