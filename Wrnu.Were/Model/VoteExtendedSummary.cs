namespace Wrnu.Were.Model;

public record VoteExtendedSummary(string name)
{
    public int votes { get; set; }
	
    public int firstVotePosition { get; set; }

    public List<Voter> voters { get; set; } = new List<Voter>();
}