namespace Wrnu.Were.Model;

public record Game(string Name, string? GameMaster, List<ForumThread> Threads, DateTime? Created) : IReadable
{
    public string FormattedString()
    {
        return $"{Name} (SL: {GameMaster})";
    }
}