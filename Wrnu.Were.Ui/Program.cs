using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Wrnu.Were.Ui;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;

var builder = WebAssemblyHostBuilder.CreateDefault(args);

builder.Services
    .AddBlazorise( options =>
    {
        options.Immediate = true;
    } )
    .AddBootstrapProviders()
    .AddFontAwesomeIcons();

builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");
var uri = builder.Configuration["Uri"];
if (string.IsNullOrWhiteSpace(uri))
{
    throw new ArgumentNullException(nameof(uri));
}
Console.WriteLine($"Url: {uri}");
builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(uri) });

await builder.Build().RunAsync();
