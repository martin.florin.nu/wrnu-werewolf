namespace Wrnu.Were.Ui.Models;

public record GameTreeNode(string DisplayName, string? Url, IEnumerable<GameTreeNode> Children, DateTime? Created);