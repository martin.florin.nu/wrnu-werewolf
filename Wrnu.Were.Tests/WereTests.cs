using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Wrnu.Were.Model;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Wrnu.Were.Tests;

public class WereTests
{
    private readonly ITestOutputHelper m_testOutputHelper;

    public WereTests(ITestOutputHelper testOutputHelper)
    {
        m_testOutputHelper = testOutputHelper;
    }

    [Fact]
    public void RemoveQuote()
    {
        var test =
            "[QUOTE=\"Sidd, post: 1104736, member: 8454\"]\nGokväll\nFramme i Götet.\nHär vare lugna tider.\n\nStrålande att vår SiarBasse lever och frodas.\nBra att få grönt på RasmusL även fast jag helst sett färg på er övriga då jag landat i Rasmus som grönast av de som jagade mig i slutet av R1.\n\nJag har inget nytt på övriga misstänkta egentligen.\nKan inte läsa av BioB eller Fjodor särskilt djup.\nDen jag tycker mig hittat nåt på är Halo som jag redovisat hit och dit.\nDet största konstiga som gör mig mkt förvånad och som i mina ögon rödar ännu mer är att Halo verkar totalt nonchalera anklagelserna som BdG tar upp och när jag lyfter det så verkar det som att Halo vidare kör tysta leken och hoppas att det ska blåsa över och att nån annan ska göra bort sig.\n\n[B]Röst: Halo9000[/B]\n[/QUOTE]\n\nBDG analys";

        var quoteregex = new Regex(@"\[QUOTE(.*?)/QUOTE\]", RegexOptions.Singleline | RegexOptions.IgnoreCase);

        var result = quoteregex.Replace(test, "<< removedQuote >>");

        Assert.Equal("<< removedQuote >>\n\nBDG analys", result);
    }

    [Theory]
    [InlineData(
        "I rödlista.\n\nRöst: [USER=1234]@Fjodorsenuba[/USER]\n\nSen så:\n\nRöst: [USER=1234]@Sidd[/USER]\n\nJag tack!",
        "Sidd")]
    [InlineData("I rödlista.\n\nRöst: [USER=1234]@Fjodorsenuba[/USER]\n\nSen så:\n\nRöst: [USER=1234]@Sidd[/USER]",
        "Sidd")]
    [InlineData("I rödlista.\n\nRöst: [USER=1234]@Fjodorsenuba[/USER]\n\nSen så:\n\nRöst: [USER=1234]@Yem_86[/USER]",
        "Yem_86")]
    [InlineData("Röst: [USER=1234]@Sidd[/USER]\n\nI rödlista.\n\nSen så:", "Sidd")]
    [InlineData("Röst:[USER=1234]@Sidd[/USER]\n\nI rödlista.\n\nSen så:", "Sidd")]
    [InlineData("I rödlista.\n\nRöst: [USER=1234]@Sidd[/USER]\n\nJag tack!", "Sidd")]
    [InlineData("Analyser \n\nSom skriva Röst: Martin3sskriptärtrasigt och.", null)]
    [InlineData(
        "\"Men hallå Basenanji, det där låter nästan som att vi ska gå ut och blotta strupen helt oförskräckt! Vad vinner vi på det?\"\n\nOm vi gör ett tankeexperiment: vad kan byn lära sig av att bybo-Basse röstas ut? Faktiskt ganska mycket! Överlevande bybor kan absolut analysera vilka som röstade på Basse, vilka som lät bli att rösta på Basse, vilka motiv som gavs för att rösta, om det är rimligt eller inte rimligt att Deltagare X i rollen som ulv medvetet röstade på en bybo eller kanske lät bli, bara för att inte stå där med bybo-blod på sina händer, mm mm.\n\nSer ni? Det är alltså inte nödvändigtvis dåligt att en bybo röstas ut i R1 om det leder till att byn kan ta ställning i det fortsatta spelet!",
        null)]
    [InlineData(
        "Ok, det viktigaste först: [SIZE=5][B]Jag har inte sett några ormar i Affärsklass A[/B][/SIZE]\n\n[QUOTE=\"Genesis, post: 1117402, member: 445\"]\n[LIST]\n[*]En sak man kan fundera på är om bybor ska avslöja endast den info som kan vara användbar för bybor. Jag tänker att bybor har ingen aning om vilka föremål som gör vad, så att säga vilka föremål som ligger på platsen ger ingen nytta. Det enda vi har nytta av är siffrorna. Så vi skulle kunna outa siffrorna på prylarna som ligger där vi står, för att göra det lättare att hitta föremål som kan låsa upp förmågor. Vad tror ni om detta?\n[/LIST]\n[/QUOTE]\n\nJag är nog inne på Basses linje här, dvs att den informationen troligtvis kommer kunna hjälpa ulvar/DTF mer än det kommer gynna oss.\n\nJag tänker gå till [B]bakre WC[/B] för att urinera men [USER=9775]@fjodorsenuba[/USER] , det kommer finnas en fin plats i [B]Affärsklass A[/B]\n\n[QUOTE=\"Empiricus, post: 1117414, member: 7501\"]\nDet låter som ett inte orimligt antagande. Alternativet skulle då vara att det antingen är någon annan förmåga som sker då eller att någon i spelet automatisk händelse sker då.\n\nVi är just nu på 10 000 fot över havet, spelar flufftänk någon roll så kanske vi börjar störta om ingen pilot finns i cockpit mellan rundorna.\n[/QUOTE]\n\nMycket intressant iakttagelse, jag gör en gissning då att vi har en DTF som ska sänka planet. Hur många piloter har vi eller är det för riskabelt att outa sådant?\n\nVad tror ni om nattmord, kommer det börja ske direkt eller kommer det vara något som ulvarna måste plocka ihop först? Just nu känns det som att det kommer ta lång tid innan någon har ett föremål klart. I bästa fall är det i runda 3 man kan använda sitt första föremål om man har flyt med siffrorna. Hur har de tidigare varit, är det nattmord direkt eller kommer det efterhand?\n\nJag tycker också att det var rätt uppenbart att man \"plussar\" i mitt PM så jag höjer upp Emipiricus för tillfället.\n\n[B]Röst: [USER=7501]@Empiricus[/USER][/B]",
        "Empiricus")]
    public void RegexTester(string message, string? expectedVote)
    {
        var forumPost = new ForumPost(message, "TESTPilot", 5, 1);

        var vote = WereVote.Parse(forumPost, "Röst");

        if (expectedVote == null)
        {
            Assert.Null(vote);
        }
        else
        {
            Assert.NotNull(vote);
            Assert.Equal(expectedVote, vote?.Vote);
        }
    }

    [Theory]
    [InlineData("Varför håller du mig som rödare än Impetigo och Fjodor?\n\n\n\nHar du ett case så kör på!", null)]
    [InlineData("I rödlista.\n\nRöst: [USER=1234]@Fjodorsenuba[/USER]\n\nSen så:\n\nRöst: [USER=1234]@Sidd[/USER]",
        "Sidd")]
    [InlineData("I rödlista.\n\nRöst: [USER=1234]@Fjodorsenuba[/USER]\n\nSen så:\n\nRöst: [USER=1234]@Yem_86[/USER]",
        "Yem_86")]
    [InlineData("Röst: [USER=1234]@Sidd[/USER]\n\nI rödlista.\n\nSen så:", "Sidd")]
    [InlineData("Röst:[USER=1234]@Sidd[/USER]\n\nI rödlista.\n\nSen så:", "Sidd")]
    [InlineData("I rödlista.\n\nRöst: [USER=1234]@Sidd[/USER]\n\nJag tack!", "Sidd")]
    [InlineData("Analyser \n\nSom skriva Röst: Martin3sskriptärtrasigt", "Martin3sskriptärtrasigt")]
    public void RegexTesterSimpleVote(string message, string? expectedVote)
    {
        var forumPost = new ForumPost(message, "TESTPilot", 5, 1);

        var vote = WereVote.Parse(forumPost, "Röst", true);

        if (expectedVote == null)
        {
            Assert.Null(vote);
        }
        else
        {
            Assert.NotNull(vote);
            Assert.Equal(expectedVote, vote?.Vote);
        }
    }

#if DEBUG
    [Fact]
#endif
    public async Task ManualTestVoting()
    {
        var client = new HttpClient();
        client.BaseAddress = new Uri("https://rollspel.nu/api/");
        client.DefaultRequestHeaders.Add("XF-Api-Key", "v1BvRNibRGCQsMQNAzcSFxY_1JHyieiY");

        var wereVote = new WrnuRestApi(client, new MemoryCache(new MemoryCacheOptions()));

        var votes = await wereVote.GetCustomVoteSummaries(79193, null, true);
        
        Assert.Equal(3, votes.Count);
    }
}