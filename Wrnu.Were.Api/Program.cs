
using Microsoft.Extensions.Caching.Memory;
using Wrnu.Were;

var builder = WebApplication.CreateBuilder(args);

var client = new HttpClient();

var envUri = builder.Configuration["ApiUri"];
var envApiKey = builder.Configuration["ApiKey"];

if (string.IsNullOrWhiteSpace(envUri))
{
    throw new Exception("ApiUri is missing");
}

if (string.IsNullOrWhiteSpace(envApiKey))
{
    throw new Exception("ApiKey is missing");
}

client.BaseAddress = new Uri(envUri);

client.DefaultRequestHeaders.Add("XF-Api-Key", envApiKey);

builder.Services.AddSingleton(client);
builder.Services.AddScoped<WrnuRestApi>();
builder.Services.AddSingleton<IMemoryCache, MemoryCache>();
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
app.UseAuthorization();

app.MapControllers();

app.Run();