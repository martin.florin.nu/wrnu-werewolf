using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Wrnu.Were.Model;

namespace Wrnu.Were.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class WerewolfController : ControllerBase
{
    private readonly ILogger<WerewolfController> _logger;
    private readonly WrnuRestApi _wrnuRestApi;

    public WerewolfController(ILogger<WerewolfController> logger, WrnuRestApi wrnuRestApi)
    {
        _logger = logger;
        _wrnuRestApi = wrnuRestApi ?? throw new ArgumentNullException(nameof(wrnuRestApi));
    }

    [HttpGet]
    public async Task<List<VoteSummary>> GetVoteSummary(int threadId, [FromQuery] bool simpleSearch = false)
    {
        return await _wrnuRestApi.GetVoteSummaries(threadId, "Röst", simpleSearch);
    }
    
    [HttpGet("GetCustomVotes")]
    public async Task<List<VoteResult>> GetCustomVotesSummary([FromQuery] int threadId, [FromQuery] string[]? attributes, [FromQuery] bool simpleSearch = false)
    {
        var result = await _wrnuRestApi.GetCustomVoteSummaries(threadId, attributes, simpleSearch);
        
        return result;
    }


    [HttpGet("GetVotes")]
    public async Task<List<VoteExtendedSummary>> GetVotes(int threadId, [FromQuery] bool simpleSearch = false)
    {
        return await _wrnuRestApi.GetExtendedVotes(threadId, "Röst", simpleSearch);
    }

    [HttpGet("GetGames/{forumId}")]
    public async Task<List<Game>> GetGames(int forumId)
    {
        return await _wrnuRestApi.GetGames(forumId);
    }
}